package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// go run main.go 80

func main() {
	port := "80"
	args := os.Args
	if len(args) > 1 {
		port = args[1]
	}

	repo := NewRepo()
	handler := NewHandler(repo)

	log.Println(fmt.Sprintf("starting server at :%s", port))

	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), handler); err != nil {
		log.Printf("error listenAndServer: %v", err)
	}
}

type fifo []string

func (f *fifo) Push(x string) {
	*f = append(*f, x)
}

func (f *fifo) Pop() string {
	x := (*f)[len(*f)-1]
	*f = (*f)[:len(*f)-1]
	return x
}

type lifo []*chan string

func (l *lifo) Push(x *chan string) {
	*l = append(*l, x)
}

func (l *lifo) Pop() *chan string {
	x := (*l)[0]
	*l = (*l)[1:]
	return x
}

type Repo struct {
	queues  map[string]*fifo
	qmu     *sync.Mutex
	waiters map[string]*lifo
	wmu     *sync.Mutex
}

func NewRepo() *Repo {
	return &Repo{
		queues:  make(map[string]*fifo),
		qmu:     &sync.Mutex{},
		waiters: make(map[string]*lifo),
		wmu:     &sync.Mutex{},
	}
}

func NewHandler(repo *Repo) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			getHandler(w, r, repo)
		case http.MethodPut:
			putHandler(w, r, repo)
		default:
			jsonError(w, "wrong method", http.StatusBadRequest)
		}
	})
}

func getHandler(w http.ResponseWriter, r *http.Request, repo *Repo) {
	queueName := strings.Trim(r.URL.Path, "/")
	timeout := r.URL.Query().Get("timeout")

	repo.qmu.Lock()
	if queue, ok := repo.queues[queueName]; ok && len(*queue) > 0 {
		value := queue.Pop()
		jsonAnswer(w, value)
		repo.qmu.Unlock()
		return
	}
	repo.qmu.Unlock()

	if timeout == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	seconds, err := strconv.Atoi(timeout)
	if err != nil {
		jsonError(w, "Wrong format of timeout", http.StatusBadRequest)
		return
	}
	ch := make(chan string)
	repo.wmu.Lock()
	if waiters, ok := repo.waiters[queueName]; ok {
		waiters.Push(&ch)
	} else {
		repo.waiters[queueName] = &lifo{&ch}
	}
	repo.wmu.Unlock()

	select {
	case value := <-ch:
		jsonAnswer(w, value)
	case <-time.After(time.Second * time.Duration(seconds)):
		ch = nil
		w.WriteHeader(http.StatusNotFound)
	}
}

func putHandler(w http.ResponseWriter, r *http.Request, repo *Repo) {
	queueName := strings.Trim(r.URL.Path, "/")
	if queueName == "" {
		jsonError(w, "empty query name", http.StatusBadRequest)
		return
	}
	value := r.URL.Query().Get("v")
	if value == "" {
		jsonError(w, "empty value", http.StatusBadRequest)
		return
	}

	repo.wmu.Lock()
	if waiters, ok := repo.waiters[queueName]; ok && len(*waiters) > 0 {
		var ch *chan string
		for len(*waiters) > 0 {
			ch = waiters.Pop()
			if *ch == nil {
				continue
			}
			*ch <- value
			break
		}
		repo.wmu.Unlock()
		w.WriteHeader(http.StatusOK)
		w.Write(nil)
		return
	}
	repo.wmu.Unlock()

	repo.qmu.Lock()
	defer repo.qmu.Unlock()
	if queue, ok := repo.queues[queueName]; ok {
		queue.Push(value)
		w.WriteHeader(http.StatusOK)
		return
	} else {
		repo.queues[queueName] = &fifo{value}
		w.WriteHeader(http.StatusOK)
		return
	}
}

func jsonError(w http.ResponseWriter, msg string, status int) {
	w.WriteHeader(status)
	w.Write([]byte(fmt.Sprintf("{\"message\":\"%s\"}", msg)))
}

func jsonAnswer(w http.ResponseWriter, value string) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("{\"value\":\"%s\"}", value)))
}
